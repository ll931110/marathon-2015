"""
Testing scheme (20 tests):
- 1 example tests (hardcoded)
- 5 tests with N = 2, 3
- 9 random tests, with N randomly in (10, 30)
- 5 random tests with N = 30
"""

import random
import subprocess
import os
import filecmp
import argparse

PATH = 'tests/'
SANITY = 'sanity/'
OPTIMAL = 'soln.py'

def low_N():
	return random.randint(2, 3)

def high_N():
	return random.randint(10, 30)

def max_N():
	return 30

def gen_random_data(gen_N):
	N = gen_N()
	sumB = 0
	info = []
	for i in range(N):
		init = random.randint(1, 30)
		down = random.randint(1, 29)
		up = random.randint(down + 1, 30)
		info.append((init, down, up))
		sumB += (30 - init) * up

	B = random.randint(1, sumB)
	return ((N, B), info)

def write_test(id, dest, data):
	(N, B), info = data
	input_name = dest + str(id) + ".in"	
	with open(input_name, 'wb') as f:
		f.write("%d %d\n" % (N, B))
		for item in info:
			f.write("%d %d %d\n" % (item[0], item[1], item[2]))
		f.close()

def run_program(id, dest, program):
	"""
	Return the output's full path
	"""
	input_name = dest + str(id) + ".in"
	input_file_object = open(input_name, 'rb')

	output_extension = "." + program
	if program == OPTIMAL:
		output_extension = ".out"	
	
	output_name = dest + str(id) + output_extension
	output_file_object = open(output_name, 'wb')
	os.system("pypy " + program + " < " + input_name + " > " + output_name)
	return output_name

def gen_example():
	data_0 = ( (2, 1), [(5, 2, 5), (1, 2, 3)] )
	write_test(0, PATH, data_0)	
	run_program(0, PATH, OPTIMAL)

def gen_buck(gen_N, dest, id_low, id_high, check=False):
	for i in range(id_low, id_high + 1):
		print i

		data = gen_random_data(gen_N)
		write_test(i, dest, data)
		output = run_program(i, dest, OPTIMAL)

		if check:
			bp_output = run_program(i, dest, BRUTE_FORCE)
			if not filecmp.cmp(output, bp_output):
				print "Error found in test generation"
				raise ValueError				

def sanity_check():
	gen_buck(high_N, ask_and_update, SANITY, 0, 999, check=True)

if __name__ == "__main__":
	gen_example()
	gen_buck(low_N, PATH, 1, 4)
	gen_buck(high_N, PATH, 5, 14)
	gen_buck(max_N, PATH, 15, 19)