#include <cstdio>
#include <cmath>
#include <cstring>
#define MAXN 35
#define MAXB 27000
#define MAXK 2 * MAXB
#define MAX_RANGE 30
using namespace std;

int A[MAXN], U[MAXN], D[MAXN], result[MAXN];
double dp[MAXN][MAXK + 10]; // the DP function shifts indices by MAXB
int val[MAXN][MAXK + 10], previous[MAXN][MAXK + 10];
double LOG[MAXN];
double INF = 1e9;
int N, B;

int main() {
	// the amount of money should always be in between [-MAXB, MAXB]
	for (int i = 1; i < MAXN; i++) {
		LOG[i] = log(i);
	}

	scanf("%d %d", &N, &B);
	for (int i = 1; i <= N; i++) {
		scanf("%d %d %d", &A[i], &D[i], &U[i]);
	}

	for (int i = 0; i <= N; i++)
		for (int j = 0; j <= MAXK; j++) {
			dp[i][j] = -INF;
		}
	dp[0][B + MAXB] = 0;

	for (int i = 0; i < N; i++) {
		for (int j = 0; j <= MAXK; j++) if (dp[i][j] > -INF) {
			for (int k = 1; k <= MAX_RANGE; k++) {
				int tmp;
				if (k < A[i + 1]) {
					// downgrade to k
					tmp = j + (A[i + 1] - k) * D[i + 1];
				}
				else {
					// upgrade
					tmp = j - (k - A[i + 1]) * U[i + 1];
				}

				if (0 <= tmp && tmp <= MAXK && dp[i + 1][tmp] < dp[i][j] + LOG[k]) {
					dp[i + 1][tmp] = dp[i][j] + LOG[k];
					val[i + 1][tmp] = k;
					previous[i + 1][tmp] = j;
				}
			}
		}
	}

	double maxResult = -INF;
	int maxPos = -1;
	for (int i = MAXB; i <= MAXK; i++) {
		if (maxResult < dp[N][i]) {
			maxResult = dp[N][i];
			maxPos = i;
		}
	}

	for (int i = N; i; i--) {
		// printf("%d %d %.3lf\n", i, maxPos, dp[i][maxPos]);
		result[i] = val[i][maxPos];
		maxPos = previous[i][maxPos];
	}

	for (int i = 1; i <= N; i++) {
		printf("%d\n", result[i]);
	}
	return 0;
}