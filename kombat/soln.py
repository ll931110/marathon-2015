#!/usr/bin/env python
if __name__ == "__main__":
	line = raw_input().split()
	N = int(line[0])
	B = int(line[1])

	init, down, up = [0] * (N + 1), [0] * (N + 1), [0] * (N + 1)
	for i in range(1, N + 1):
		line = raw_input().split()
		init[i], down[i], up[i] = int(line[0]), int(line[1]), int(line[2])

	MAX_K = 30
	MAX_RANGE = 30*30*30

	dp, trace, values = {}, {}, {}
	dp[(0, B)] = 1
	for i in range(0, N):
		for j in range(-MAX_RANGE, MAX_RANGE + 1):
			if (i, j) in dp:
				for k in range(1, MAX_K + 1):
					if k > init[i + 1]:
						newPrice = j - up[i + 1] * (k - init[i + 1])
					else:
						newPrice = j + down[i + 1] * (init[i + 1] - k)

					prod = dp[(i, j)] * k
					if (i + 1, newPrice) not in dp or dp[(i + 1, newPrice)] < prod:
						dp[(i + 1, newPrice)] = prod
						values[(i + 1, newPrice)] = k
						trace[(i + 1, newPrice)] = j

	maxProduct, maxPos = max([(dp[(N, i)], i) for i in range(0, MAX_RANGE + 1) if (N, i) in dp])
	selected = [0] * (N + 1)
	idx = N
	while idx > 0:
		selected[idx] = values[(idx, maxPos)]
		maxPos = trace[(idx, maxPos)]
		idx -= 1

	for i in range(1, N + 1):
		print selected[i]