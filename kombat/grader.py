import os

def read_product(file):
	f = open(file, 'rb')
	prod = 1
	for line in f:
		x = int(line)
		prod *= x
	return prod

if __name__ == "__main__":
	print "Building solution"
	os.system("g++ soln.cpp -O2 -o optimal")
	for c in range(20):
		os.system("./optimal < tests/" + str(c) + ".in" + " > output.txt")
		if read_product("tests/" + str(c) + ".out") != read_product("output.txt"):
			raise ValueError