g++ soln.cpp -O2 -o soln
g++ sol_rr.cpp -O2 -o sol_rr

for ((c = 0; c < 20; c++)); do
	echo $c
	./soln < tests/$c.in > ans.txt
	./sol_rr < tests/$c.in > greed.txt
	diff -w ans.txt greed.txt
done

rm -rf ans.txt
rm -rf greed.txt
