"""
Testing scheme (20 tests):
- 2 example tests (hardcoded)
- 2 tests with N, Q <= 50
- 4 tests with N, Q <= 10^5, only queries of type 2
- 8 tests with 10^4 <= N, Q <= 10^5, random data
- 4 tests with N, Q = 10^5
"""

import random
import subprocess
import os
import filecmp
import argparse

PATH = 'tests/'
SANITY = 'sanity/'
OPTIMAL = 'optimal'
BRUTE_FORCE = 'brute_force'

def tiny_N():
	N = random.randint(3, 5)
	Q = random.randint(3, 5)
	return (N, Q)

def low_N():
	N = random.randint(3, 100)
	Q = random.randint(1, 100)
	return (N, Q)

def high_N():
	N = random.randint(10000, 100000)
	Q = random.randint(10000, 100000)
	return (N, Q)

def max_N():
	return (100000, 100000)

def ask_only():
	return 2

def ask_and_update():
	return random.randint(1, 2)

def gen_random_data(gen_N, gen_query):
	N, Q = gen_N()
	
	initial_array = [random.randint(-10**6, 10**6) for i in range(N)]

	info = []
	for i in range(Q):
		typ = gen_query()
		if typ == 1:
			x = random.randint(0, N - 1)
			y = random.randint(-10**6, 10**6)
		else:
			x = random.randint(0, N - 3)
			y = random.randint(x + 2, N - 1)
		info.append((typ, x, y))
	return ((N, Q), initial_array, info)

def write_test(id, dest, data):
	(N, Q), initial_array, info = data
	input_name = dest + str(id) + ".in"	
	with open(input_name, 'wb') as f:
		f.write("%d %d\n" % (N, Q))
		for i in range(N):
			f.write("%d" % initial_array[i])
			if i == N - 1:
				f.write("\n")
			else:
				f.write(" ")

		for item in info:
			f.write("%d %d %d\n" % (item[0], item[1], item[2]))

		f.close()

def run_program(id, dest, program):
	"""
	Return the output's full path
	"""
	input_name = dest + str(id) + ".in"
	input_file_object = open(input_name, 'rb')

	output_extension = "." + program
	if program == OPTIMAL:
		output_extension = ".out"	
	
	output_name = dest + str(id) + output_extension
	output_file_object = open(output_name, 'wb')
	os.system("./" + program + " < " + input_name + " > " + output_name)
	return output_name

def gen_example():
	data_0 = ( (3, 3), [1, 2, 3], [(2, 0, 2), (1, 2, -1), (2, 0, 2)] )
	write_test(0, PATH, data_0)	
	run_program(0, PATH, OPTIMAL)

	data_1 = ( (5, 5), [3, -1, 4, -1, 5], [(2, 0, 4), (2, 1, 3), (1, 2, -6), (1, 4, -3), (2, 2, 4)] )
	write_test(1, PATH, data_1)
	run_program(1, PATH, OPTIMAL)

def gen_buck(gen_N, gen_query, dest, id_low, id_high, check=False):
	for i in range(id_low, id_high + 1):
		print i

		data = gen_random_data(gen_N, gen_query)
		write_test(i, dest, data)
		output = run_program(i, dest, OPTIMAL)

		if check:
			bp_output = run_program(i, dest, BRUTE_FORCE)
			if not filecmp.cmp(output, bp_output):
				print "Error found in test generation"
				raise ValueError				

def sanity_check():
	gen_buck(low_N, ask_and_update, SANITY, 0, 999, check=True)

if __name__ == "__main__":
	print "Building solution"
	os.system("g++ soln.cpp -O2 -o " + OPTIMAL)
	os.system("g++ brute_force.cpp -O2 -o " + BRUTE_FORCE)

	gen_example()
	gen_buck(low_N, ask_and_update, PATH, 2, 3)
	gen_buck(high_N, ask_only, PATH, 4, 7)
	gen_buck(high_N, ask_and_update, PATH, 8, 15)
	gen_buck(max_N, ask_and_update, PATH, 16, 19)