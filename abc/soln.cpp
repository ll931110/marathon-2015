#include <algorithm>
#include <cstdio>
#include <cmath>
#define MAXN 100005
#define MAX_RANGE 3
#define MIN_RANGE 2
using namespace std;

int N, Q;
int A[MAXN];
int INF = 1 << 28;
long long INF_VALUE = (1LL << 60);

long long evaluateTriple(pair<int,int> p1, pair<int,int> p2, pair<int,int> p3) {
	if (p1.second == p2.second || p1.second == p3.second || p2.second == p3.second) {
		return -INF_VALUE;
	}
	if (p1.second < 0 || p2.second < 0 || p3.second < 0) {
		return -INF_VALUE;
	}

	return 1LL * p1.first * p2.first + p3.first;
}

struct ReducedInterval {
	// Both maxValue and minValue will be stored by the increasing value of elements
	// Break ties by indices

	pair<int,int> maxValue[MAX_RANGE];
	pair<int,int> minValue[MIN_RANGE];

	ReducedInterval() {
		for (int i = 0; i < MAX_RANGE; i++) {
			maxValue[i] = make_pair(-INF, -1);
		}
		for (int i = 0; i < MIN_RANGE; i++) {
			minValue[i] = make_pair(INF, -1);
		}
	}

	void set(int val, int pos) {
		// the pair (val, pos) is guaranteed not to appear upfront
		pair<int,int> candidate = make_pair(val, pos);
		for (int i = MAX_RANGE - 1; i >= 0; i--) {
			if (candidate > maxValue[i]) {
				// shift values by 1
				for (int j = 0; j < i; j++) {
					maxValue[j] = maxValue[j + 1];
				}
				maxValue[i] = candidate;
				break;
			}
		}
		for (int i = 0; i < MIN_RANGE; i++) {
			if (candidate < minValue[i]) {
				for (int j = MIN_RANGE - 1; j > i; j--) {
					minValue[j] = minValue[j - 1];
				}
				minValue[i] = candidate;
				break;
			}
		}
	}

	struct ReducedInterval merge(struct ReducedInterval other) {
		// this and other are guaranteed to be disjoint
		ReducedInterval newInterval = ReducedInterval();

		for (int i = MAX_RANGE - 1, j = MAX_RANGE - 1, k = MAX_RANGE - 1; k >= 0; k--) {
			if (maxValue[i] > other.maxValue[j]) {
				newInterval.maxValue[k] = maxValue[i];
				i--;
			}
			else {
				newInterval.maxValue[k] = other.maxValue[j];
				j--;
			}
		}

		for (int i = 0, j = 0, k = 0; k < MIN_RANGE; k++) {
			if (minValue[i] < other.minValue[j]) {
				newInterval.minValue[k] = minValue[i];
				i++;
			}
			else {
				newInterval.minValue[k] = other.minValue[j];
				j++;
			}
		}
		return newInterval;
	}

	long long evaluateSum() {
		long long ans = evaluateTriple(minValue[0], minValue[1], maxValue[2]);
		ans = max(ans, evaluateTriple(maxValue[0], maxValue[1], maxValue[2]));
		ans = max(ans, evaluateTriple(maxValue[1], maxValue[2], maxValue[0]));
		ans = max(ans, evaluateTriple(maxValue[0], maxValue[2], maxValue[1]));
		return ans;
	}

	void print() {
		for (int i = 0; i < MAX_RANGE; i++) {
			printf("%d ", maxValue[i].first);
		}
		printf("\n");
		for (int i = 0; i < MIN_RANGE; i++) {
			printf("%d ", minValue[i].first);
		}
		printf("\n");
	}
};

ReducedInterval intev[4 * MAXN];

void init(int i, int low, int high) {
	intev[i] = ReducedInterval();
	if (low == high) {
		intev[i].set(A[low], low);
		// printf("id %d %d\n", low, high);
		// intev[i].print();
		return;
	}
	int mid = (low + high) >> 1;
	init(i << 1, low, mid);
	init((i << 1) + 1, mid + 1, high);
	intev[i] = intev[i << 1].merge(intev[(i << 1) + 1]);

	// printf("id %d %d\n", low, high);
	// intev[i].print();
}

void update(int i, int low, int high, int x, int p) {
	if (low == high) {
		intev[i] = ReducedInterval();
		intev[i].set(p, low);
		return;
	}
	int mid = (low + high) >> 1;
	if (x <= mid) {
		update(i << 1, low, mid, x, p);
	}
	else {
		update((i << 1) + 1, mid + 1, high, x, p);
	}
	intev[i] = intev[i << 1].merge(intev[(i << 1) + 1]);
}

struct ReducedInterval get(int i, int low, int high, int u, int v) {
	if (v < low || high < u) {
		// the default interval is null
		return ReducedInterval();
	}
	if (u <= low && high <= v) {
		return intev[i];
	}
	int mid = (low + high) >> 1;
	return get(i << 1, low, mid, u, v).merge(get((i << 1) + 1, mid + 1, high, u, v));
}

int main() {
	scanf("%d %d", &N, &Q);
	for (int i = 0; i < N; i++) {
		scanf("%d", &A[i]);
	}

	init(1, 0, N - 1);
	while (Q--) {
		int typ, x, y;
		scanf("%d %d %d", &typ, &x, &y);
		if (typ == 1) {
			update(1, 0, N - 1, x, y);
		}
		else {
			printf("%lld\n", get(1, 0, N - 1, x, y).evaluateSum());
		}
	}
	return 0;
}