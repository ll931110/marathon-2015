/*
	Brute Force solution: O(N^3) per query
*/

#include <cmath>
#include <cstdio>
#include <cstdlib>
#define MAXN 100005
using namespace std;

int N, Q;
int A[MAXN];
long long INF = 1LL << 62;

int main() {
	scanf("%d %d", &N, &Q);
	for (int i = 0; i < N; i++) {
		scanf("%d", &A[i]);
	}
	while (Q--) {
		int typ, x, y;
		scanf("%d %d %d", &typ, &x, &y);
		if (typ == 1) {
			A[x] = y;
		}
		else {
			long long ans = -INF;
			for (int i = x; i <= y; i++) {
				for (int j = x; j <= y; j++) {
					for (int k = x; k <= y; k++) {
						if (i != j && i != k && j != k) {
							long long tmp = 1LL * A[i] * A[j] + A[k];
							if (ans < tmp) {
								ans = tmp;
							}
						}
					}
				}
			}
			printf("%lld\n", ans);
		}
	}
	return 0;
}